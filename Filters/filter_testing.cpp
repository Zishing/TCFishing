#include <bits/stdc++.h>
#include "../Decoder/AISFormat.h"
using namespace std;

string parseDate(long long timestamp) {
  time_t now = timestamp;
  struct tm *ptm;

  ptm = gmtime(&now);

  char buffer [80];
  int l = sprintf(buffer, "%d,%d,%d,%d", ptm->tm_yday, ptm->tm_mon, ptm->tm_hour, ptm->tm_min);
  buffer[l] = 0;

  return string(buffer);
}

vector <string> parseLine(string line, char c = ',') {
	vector <string> ans;

	bool inString = false;
	string cur = "";

	for (int i = 0; i < line.size(); i++) {
		if (line[i] == '"') inString ^= 1;
		if (line[i] == c && !inString) {
			ans.push_back(cur);
			cur = "";
		} else {
			cur += line[i];
		}
	}
	ans.push_back(cur);

	return ans;
}

long long stoLong(string number) {
	long long ans = 0;
	for (int i = 0; i < number.size(); i++) {
		ans = ans*10 + (number[i] - '0');
	}
	return ans;
}

int main() {
	vector <string> parsed;

	string line;
	getline(cin, line);

	cout << "YearDay,Month,Hour,Minute,MESSAGE_ID,MMSI,LAT,LONG,POS_ACCURACY,NAV_STATUS,SOG,COG,TRUE_HEADING";
	cout << endl;

	while (getline(cin, line)) {
		parsed = parseLine(line);
		cout << parseDate(stoLong(parsed[0]));
    string raw_message;
    string s=parsed[1];
    for(int i=0; i <(int)s.size();i++){
      if(s[i]=='"'){
        for(int j=i+1;s[j]!='"';j++)raw_message+=s[j];
        break;
      }
    }
    AIS deco = decoder(raw_message);
    s.erase(s.size()-1);
    cout<<","<<deco.MessageType<<","<<deco.MMSI<<",";
    printf("%.6f,",deco.Latitude);
    printf("%.6f",deco.Longitude);
    cout<<","<<deco.Accuracy<<","<<deco.NavigationStatus;
    if (deco.TrueHeading == 511) {
        deco.TrueHeading = deco.COG;
    }
    cout<<","<<deco.SOG<<","<<deco.COG<<","<<deco.TrueHeading<<endl;
	}
}
