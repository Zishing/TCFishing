#include <bits/stdc++.h>

using namespace std;

string parseDate(long long timestamp) {
  time_t now = timestamp;
  struct tm *ptm;

  ptm = gmtime(&now);

  char buffer [80];
  int l = sprintf(buffer, "%d,%d,%d,%d", ptm->tm_yday, ptm->tm_mon, ptm->tm_hour, ptm->tm_min);
  buffer[l] = 0;

  return string(buffer);
}

vector <string> parseLine(string line, char c = ',') {
	vector <string> ans;

	bool inString = false;
	string cur = "";

	for (int i = 0; i < line.size(); i++) {
		if (line[i] == '"') inString ^= 1;
		if (line[i] == c && !inString) {
			ans.push_back(cur);
			cur = "";
		} else {
			cur += line[i];
		}
	}
	ans.push_back(cur);

	return ans;
}

long long stoLong(string number) {
	long long ans = 0;
	for (int i = 0; i < number.size(); i++) {
		ans = ans*10 + (number[i] - '0');
	}
	return ans;
}

int main() {
	vector <string> parsed;

	string line;
	getline(cin, line);

	cout << "YearDay,Month,Hour,Minute";
	parsed = parseLine(line);

	for (int i = 2; i < parsed.size(); i++){
		if (i == 11) continue;
		cout << "," << parsed[i];
	}
	cout << endl;

	getline(cin, line);
	while (getline(cin, line)) {
		parsed = parseLine(line);

		cout << parseDate(stoLong(parsed[0]));
		if (parsed[10] == "" || parsed[10] == "0" || parsed[10] == "511") {
			parsed[10] = parsed[9];
		}
		/*char fc = parsed[12][0];
		if (fc == 'F') {
			parsed[12] = "0.75";
		} else if (fc == 'N') {
			parsed[12] = "0.25";
		} else {
			parsed[12] = "0.5";
		}
        */
		for (int i = 2; i < parsed.size(); i++){
			if (i == 11) continue;
			cout << "," << parsed[i];
		}
		cout << endl;
	}
}
