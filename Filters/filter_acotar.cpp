#include <bits/stdc++.h>

using namespace std;

int main() {
	double d = 0, mn = 10, mx = -10;
	vector <double> vals;
	while (cin >> d) {
		vals.push_back(d);
		mn = min(mn, d);
		mx = max(mx, d);
	}

	mx -= mn;
	for (int i = 0; i < vals.size(); i++) {
		d = (vals[i] - mn)/mx;
		printf("%.5f\n", d);
	}
}
