#include <bits/stdc++.h>

using namespace std;

vector <string> parseLine(string line, char c = ',') {
	vector <string> ans;

	bool inString = false;
	string cur = "";

	for (int i = 0; i < line.size(); i++) {
		if (line[i] == '"') inString ^= 1;
		if (line[i] == c && !inString) {
			ans.push_back(cur);
			cur = "";
		} else {
			cur += line[i];
		}
	}
	ans.push_back(cur);

	return ans;
}

double strToDouble(string str){
  istringstream out(str);
  double ans;
  out >> ans;
  return ans;
}

int strToInt(string str) {
	int ans = 0;
	for (int i = 0; i < str.size(); i++) {
		ans = ans*10 + (str[i] - '0');
	}
}

int navs[] = {1, 2, 4, 6, 10, 11, 14};

double getProb(vector <string> &cols) {
	double sog = strToDouble(cols[10]);
	if (sog > 8) return 0.0;

	int ns = strToInt(cols[9]);
	for (int i = 0; i < 7; i++) {
		if (navs[i] == ns) return 0.0;
	}

	double th = strToDouble(cols[12]);
	if (th > 360) {
		return 0.0;
	}

	double lat = strToDouble(cols[6]);
	if (lat < -5 || (lat > 0 && lat < 40) || lat > 60) {
		return 0.0;
	}

	double lng = strToDouble(cols[7]);
	if (lng < -170 || (lng > -120 && lng < 145) || lng > 175) {
		return 0.0;
	}

	return 10.0;
}

int main() {
	string line;
	vector <string> cols;

	getline(cin, line);
	cout << line << ",DNF" << endl;

	while (getline(cin, line)) {
		cols = parseLine(line);

		double prob = getProb(cols);
		cout << line << "," << prob << endl;
	}
}
