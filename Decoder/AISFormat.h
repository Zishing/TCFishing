#include <bits/stdc++.h>

using namespace std;

vector<string> parseRawMess(string s) {
  vector<string> parsed;
  stringstream ss;
  ss.str(s);
  string item;
  while (getline(ss, item, ',')) {
    parsed.push_back(item);
  }
  return parsed;
}

bitset<6> to6Bit(char c) {
  bitset<6> foo;
  int intc = c - 48;
  if (intc > 40) intc -= 8;
  int pos = 0;
  while (pos < 6) {
    if (intc & 1)
      foo.set(pos);
    else
      foo.set(pos, 0);
    pos++;
    intc /= 2;
  }
  return foo;
}

bitset<168> allToBits(string s) {
  bitset<168> ans;
  ans.reset();
  int pos = 0;
  for (int i = 0; i < (int)s.size(); i++) {
    bitset<6> bs = to6Bit(s[i]);
    for (int j = 5; j >= 0; j--) {
      ans[pos] = bs[j];
      pos++;
    }
  }
  // return the reverse set of bitset
  return ans;
}

bitset<312> allToBits2(string s) {
  bitset<312> ans;
  ans.reset();
  int pos = 0;
  for (int i = 0; i < (int)s.size(); i++) {
    bitset<6> bs = to6Bit(s[i]);
    for (int j = 5; j >= 0; j--) {
      ans[pos] = bs[j];
      pos++;
    }
  }
  // return the reverse set of bitset
  return ans;
}

bitset<96> allToBits3(string s) {
  bitset<96> ans;
  ans.reset();
  int pos = 0;
  for (int i = 0; i < (int)s.size(); i++) {
    bitset<6> bs = to6Bit(s[i]);
    for (int j = 5; j >= 0; j--) {
      ans[pos] = bs[j];
      pos++;
    }
  }
  // return the reverse set of bitset
  return ans;
}

struct AIS {
  int MessageType;
  int RepeatIndicator;
  int MMSI;
  int NavigationStatus;
  int ROT;
  double SOG;
  bool Accuracy;
  double Longitude, Latitude;
  double COG;
  int TrueHeading, TimeStamp;
  int ManeuverIndicator;
  bitset<3> Spare;
  bool RAIMFlag;
  int Radio;
};

void printAIS(AIS mess) {
  cout << setprecision(9);
  cout << "Message Type:" << mess.MessageType << endl;
  cout << "MMSI:" << mess.MMSI << endl;
  cout << "Nav. Status: " << mess.NavigationStatus << endl;
  cout << "SOG: " << mess.SOG << endl;
  cout << "Accuracy: " << mess.Accuracy << endl;
  cout << "Longitude: " << mess.Longitude << endl;
  cout << "Latitude: " << mess.Latitude << endl;
  cout << "COG: " << mess.COG << endl;
  cout << "True Heading: " << mess.TrueHeading << endl;
}

AIS decoder1(string s) {
  bitset<168> binary = allToBits(s);
  AIS ans;
  // Message Type
  int temp = 0;
  long long pot2 = 1;
  for (int i = 5; i >= 0; i--) {
    temp += binary[i] * pot2;
    pot2 *= 2;
  }
  ans.MessageType = temp;
  // MMSI 8-37
  int mmsi = 0;
  pot2 = 1;
  for (int i = 37; i >= 8; i--) {
    mmsi += binary[i] * pot2;
    pot2 *= 2;
  }
  ans.MMSI = mmsi;
  // Navigation Status
  int NS = 0;
  pot2 = 1;
  for (int i = 41; i >= 38; i--) {
    NS += binary[i] * pot2;
    pot2 *= 2;
  }
  ans.NavigationStatus = NS;
  // Speed Over Ground SOG
  double SOG = 0;
  pot2 = 1;
  for (int i = 59; i >= 50; i--) {
    SOG += binary[i] * pot2;
    pot2 *= 2;
  }
  ans.SOG = SOG / 10;
  // Position Accuracy
  ans.Accuracy = binary[60];

  // Longitude
  bitset<27> longitudebit;
  pot2 = 1;
  for (int i = 62; i <= 88; i++) {
    longitudebit[88 - i] = binary[i];
    pot2 *= 2;
  }
  int longitude = longitudebit.to_ulong();
  if (binary[61]) longitude -= pot2;
  ans.Longitude = longitude / 600000.0;

  // Latitude
  bitset<26> latitudebit;
  pot2 = 1;
  for (int i = 90; i <= 115; i++) {
    latitudebit[115 - i] = binary[i];
    pot2 *= 2;
  }
  int latitude = latitudebit.to_ulong();
  if (binary[89]) latitude -= pot2;
  ans.Latitude = latitude / 600000.0;

  // Course Over Ground COG

  pot2 = 1;
  double COG = 0;
  for (int i = 127; i >= 116; i--) {
    COG += binary[i] * pot2;
    pot2 *= 2;
  }
  ans.COG = COG / 10;

  // True Heading
  int TH = 0;
  pot2 = 1;
  for (int i = 136; i >= 128; i--) {
    TH += binary[i] * pot2;
    pot2 *= 2;
  }
  ans.TrueHeading = TH;
  return ans;
}

AIS decoder2(string s) {
  bitset<312> binary = allToBits2(s);
  AIS ans;
  // Message Type
  int temp = 0;
  long long pot2 = 1;
  for (int i = 5; i >= 0; i--) {
    temp += binary[i] * pot2;
    pot2 *= 2;
  }
  ans.MessageType = temp;
  // MMSI 8-37
  int mmsi = 0;
  pot2 = 1;
  for (int i = 37; i >= 8; i--) {
    mmsi += binary[i] * pot2;
    pot2 *= 2;
  }
  ans.MMSI = mmsi;
  // Position Accuracy
  ans.Accuracy = binary[56];

  // Navigation Status
  ans.NavigationStatus = 15;
  // Speed Over Ground SOG
  double SOG = 0;
  pot2 = 1;
  for (int i = 55; i >= 46; i--) {
    SOG += binary[i] * pot2;
    pot2 *= 2;
  }
  ans.SOG = SOG / 10;
  // Longitude
  bitset<27> longitudebit;
  pot2 = 1;
  for (int i = 58; i <= 84; i++) {
    longitudebit[84 - i] = binary[i];
    pot2 *= 2;
  }
  int longitude = longitudebit.to_ulong();
  if (binary[57]) longitude -= pot2;
  ans.Longitude = longitude / 600000.0;

  // Latitude
  bitset<26> latitudebit;
  pot2 = 1;
  for (int i = 86; i <= 111; i++) {
    latitudebit[111 - i] = binary[i];
    pot2 *= 2;
  }
  int latitude = latitudebit.to_ulong();
  if (binary[85]) latitude -= pot2;
  ans.Latitude = latitude / 600000.0;

  // Course Over Ground COG

  pot2 = 1;
  double COG = 0;
  for (int i = 123; i >= 112; i--) {
    COG += binary[i] * pot2;
    pot2 *= 2;
  }
  ans.COG = COG / 10;

  // True Heading
  int TH = 0;
  pot2 = 1;
  for (int i = 132; i >= 124; i--) {
    TH += binary[i] * pot2;
    pot2 *= 2;
  }
  ans.TrueHeading = TH;
  return ans;
}

AIS decoder3(string s) {
  bitset<96> binary = allToBits3(s);
  AIS ans;
  // Message Type
  int temp = 0;
  long long pot2 = 1;
  for (int i = 5; i >= 0; i--) {
    temp += binary[i] * pot2;
    pot2 *= 2;
  }
  ans.MessageType = temp;
  // MMSI 8-37
  int mmsi = 0;
  pot2 = 1;
  for (int i = 37; i >= 8; i--) {
    mmsi += binary[i] * pot2;
    pot2 *= 2;
  }
  ans.MMSI = mmsi;
  // Navigation Status
  int NS = 0;
  pot2 = 1;
  for (int i = 43; i >= 40; i--) {
    NS += binary[i] * pot2;
    pot2 *= 2;
  }
  ans.NavigationStatus = NS;
  // Position Accuracy
  ans.Accuracy = binary[38];
  // Longitude
  bitset<17> longitudebit;
  pot2 = 1;
  for (int i = 45; i <= 61; i++) {
    longitudebit[61 - i] = binary[i];
    pot2 *= 2;
  }
  int longitude = longitudebit.to_ulong();
  if (binary[44]) longitude -= pot2;
  ans.Longitude = longitude / 600.0;

  // Latitude
  bitset<16> latitudebit;
  pot2 = 1;
  for (int i = 63; i <= 78; i++) {
    latitudebit[78 - i] = binary[i];
    pot2 *= 2;
  }
  int latitude = latitudebit.to_ulong();
  if (binary[62]) latitude -= pot2;
  ans.Latitude = latitude / 600.0;
  // Speed Over Ground SOG
  double SOG = 0;
  pot2 = 1;
  for (int i = 84; i >= 79; i--) {
    SOG += binary[i] * pot2;
    pot2 *= 2;
  }
  ans.SOG = SOG;
  // Course Over Ground COG

  pot2 = 1;
  double COG = 0;
  for (int i = 93; i >= 85; i--) {
    COG += binary[i] * pot2;
    pot2 *= 2;
  }
  ans.COG = COG;
  ans.TrueHeading = (int)ans.COG;
  return ans;
}

AIS decoder(string s) {
  s = parseRawMess(s)[5];
  int id = (int)(to6Bit(s[0]).to_ulong());
  if (id == 18 || id == 19) {
    return decoder2(s);
  } else if (id == 27) {
    return decoder3(s);
  } else {
    return decoder1(s);
  }
}

bool checksum(string s) {
  string hexa = "0123456789ABCDEF";
  int checksum = 0;
  string check;
  for (int i = 1; i < s.size() - 3; i++) {
    checksum ^= (int)s[i];
  }
  check += (char)(hexa[checksum / 16]);
  check += (char)(hexa[checksum % 16]);
  return check == s.substr(s.size() - 2);
}

char* asctime(const struct tm* timeptr) {
  static char result[26];
  sprintf(result, "%02d/%02d/%d %.2d:%.2d::%.2d \n", timeptr->tm_mon,
          timeptr->tm_mday, 1900 + timeptr->tm_year, timeptr->tm_hour,
          timeptr->tm_min, timeptr->tm_sec);
  return result;
}

string getDate(long long timestamp) {
  time_t now = timestamp;
  struct tm* ptm;
  ptm = gmtime(&now);
  string c(asctime(ptm));
  c.erase(c.size() - 2);
  return c;
}