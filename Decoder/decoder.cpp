#include "AISFormat.h"

using namespace std;

void writeFileTraining() {
  cout << setprecision(9);
  freopen("../Data/training_data.csv", "r", stdin);
  freopen("../Data/new_tc_training_data.csv", "w", stdout);
  string s, head;
  getline(cin, s);
  s.erase(s.size() - 1);
  head =
      ",MESSAGE_ID,MMSI,LAT,LONG,POS_ACCURACY,NAV_STATUS,SOG,COG,TRUE_HEADING";
  cout << s << head << endl;
  while (getline(cin, s)) {
    string raw_message;
    for (int i = 0; i < (int)s.size(); i++) {
      if (s[i] == '"') {
        for (int j = i + 1; s[j] != '"'; j++) raw_message += s[j];
        break;
      }
    }

    AIS deco = decoder(raw_message);
    s.erase(s.size() - 1);
    cout << s << "," << deco.MessageType << "," << deco.MMSI << ",";
    printf("%.6f,", deco.Latitude);
    printf("%.6f", deco.Longitude);
    cout << "," << deco.Accuracy << "," << deco.NavigationStatus;
    cout << "," << deco.SOG << "," << deco.COG << "," << deco.TrueHeading
         << endl;
  }
}

void fileCheckSum() {
  freopen("../Data/tc_testing_data.csv", "r", stdin);
  string s, head;
  getline(cin, s);
  s.erase(s.size() - 1);
  int cont = 0;
  while (getline(cin, s)) {
    string raw_message;
    for (int i = 0; i < (int)s.size(); i++) {
      if (s[i] == '"') {
        for (int j = i + 1; s[j] != '"'; j++) raw_message += s[j];
        break;
      }
    }
    if (!checksum(raw_message)) {
      cont++;
      cout << 0 << endl;
    }
  }
  cout << "--------" << endl;
  cout << cont << endl;
}

int main() {
  string c;
  // writeFileTraining();
  fileCheckSum();
}
