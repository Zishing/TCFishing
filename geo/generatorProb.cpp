#include<bits/stdc++.h>
using namespace std;

double P[ 200 ][ 30 ][ 12 ];
string tmp, M, G, Y;
char path[ 26 ], ch;
string line ;

void getPath(){
  tmp = "barco/" + M + "/" + G + "/" + Y + ".txt";
  for( int i = 0; i < 27; i++ )
    path[ i ] = tmp[ i ];
  path[ 27 ] = '\0';
}

int getX( double longitud ){
  return ( longitud + 180 )*0.36565 + 16;
}

int getY( double latitud ){
  return ( 82.7882 - ( latitud + 14.26 ) )*0.24158 ;
}

double getProb(  double latitud, double longitud, int mes ){
  return P[ getX( longitud ) ][ getY( latitud ) ][ mes ];
}

void init(){
  memset( P , 0 , sizeof( P ) );
  for( char m = 'A'; m <= 'I'; m ++ ){
    for( char a = '3'; a <= '5'; a ++ ){
      M = "mes", G = "grafos", Y = "201";
      M += m, G += m, Y += a;
      getPath();
      freopen(path, "r", stdin);
      for( int j = 0; j <= 20; j ++ ){
        for( int i = 0; i <= 171; i ++ ){
          cin >> ch;
          P[ i ][ j ][ m - 'A' ] += (ch == 'X')*0.333;
        }
      }
      fclose(stdin);
    }
  }
  cout << "...generado con exito\n" ;
}

vector <string> parseLine(string line, char c = ',') {
	vector <string> ans;

	bool inString = false;
	string cur = "";

	for (int i = 0; i < line.size(); i++) {
		if (line[i] == '"') inString ^= 1;
		if (line[i] == c && !inString) {
			ans.push_back(cur);
			cur = "";
		} else {
			cur += line[i];
		}
	}
	ans.push_back(cur);

	return ans;
}

double strToDob( string str ){
  istringstream out( str );
  double ans;
  out >> ans;
  return ans;
}

int strToInt( string str ){
  istringstream out( str );
  int ans;
  out >> ans;
  return ans;
}

void genTraining(){
  freopen("../Data/training_filtered.csv", "r", stdin);
  freopen("../Data/training_filteredx2.csv", "w", stdout);
  getline( cin , line );
  line.erase( line.size() - 1 );
  cout << line << ",Prob_Zone_Active"<< endl ;
  while( getline( cin , line ) ){
    vector<string> rows = parseLine( line );
    int y = strToDob( rows[ 6 ] );
    int x = strToDob( rows[ 7 ] );
    int m = strToInt( rows[ 1 ] );
    line.erase((int)line.size() - 1);
    cout << line << "," << getProb( x , y , m ) << endl;
  }
  fclose(stdin);
  fclose(stdout);
}

void genTesting(){
  freopen("../Data/testing_filtered.csv", "r", stdin);
  freopen("../Data/testing_filteredx2.csv", "w", stdout);
  getline( cin , line );
  // line.erase( line.size() - 1 );
  // cout << line << endl;
  cout << line + ",Prob_Zone_Active\n";
  while( getline( cin , line ) ){
    vector<string> rows = parseLine( line );
    int y = strToDob( rows[ 6 ] );
    int x = strToDob( rows[ 7 ] );
    int m = strToInt( rows[ 1 ] );
    line.erase((int)line.size() - 1);
    cout << line + "," << getProb( x , y , m ) << '\n';
  }
  fclose(stdin);
  fclose(stdout);
}

int main(){

  init();
  genTraining();
  // genTesting();
  /*
  freopen("salida.txt", "w" , stdout);
  for( int j = 0; j <= 20; j++ ){
    for( int i = 0; i <= 171; i++ ){
      cout << P[ i ][ j ][ 3 ] ;
    }
    cout << endl;
  }
  fclose(stdout);
  */
  /*
  double latitud, longitud;
  latitud = 30.1451, longitud = -167.6953;
  int x = getX( longitud );
  int y = getY( latitud );
  cout << "_ x = " << x << endl;
  cout << "_ y = " << y << endl;
  cout << "Probabilidad de este punto en dicho mes:" << getProb( x,y,0 );
  */

  return 0;
}
